# TDS Coding Test

## Technologies and libraries

* Java 8
* gradle build
* Apache commons-csv 

## Usage

Build: `gradlew clean build`

Run application (from the project root): `java -jar build/libs/tds-assignment-0.0.1-SNAPSHOT.jar out.csv`
 
_com.tds.report.App_ performs dataset joins, report column configuration and exports resulting data into a csv file defined as a command line parameter. 
Source datasets are prepackaged as resources in the jar file.

## Assumptions and considerations

* Field names are case sensitive
* No field name duplication is expected accross different datasources except reference key fields
* Composite key support is not required
* Detail tables are joined using PK or AK, at most one record is expected in the detail table
* Dynamic list of maps was chosen over POJO models as dataset representation to provide higher level of flexibility 
* Computable fields implemented as custom formatters. Field names for such formatters are configurable
* Datasources other than csv can be attached through additional DataSource interface implementation
