package com.tds.report.formatter;

import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.TreeMap;

@Slf4j
public class MonthsFromTodayBucket implements ValueExtractor {
    private static final DateTimeFormatter FORMAT = DateTimeFormatter.ofPattern("yyyyMMdd");

    private static TreeMap<Long, String> terms = new TreeMap<>();

    static {
        terms.put(0L, "0m-1m");
        terms.put(1L, "1m-6m");
        terms.put(6L, "6m-1yr");
        terms.put(12L, "1yr-10yr");
        terms.put(120L, "10yr-30yr");
        terms.put(360L, "30yr-50yr");
        terms.put(600L, "50yr+");
    }

    @Override
    public String getValue(Map<String, String> record, String key, String ... fields) {
        String maturityDateValue = record.getOrDefault(fields[0], "");
        try {
            LocalDate today = LocalDate.now();
            LocalDate maturityDate = LocalDate.parse(maturityDateValue, FORMAT);
            if (today.compareTo(maturityDate) < 0) {
                long months = ChronoUnit.MONTHS.between(today, maturityDate);
                return terms.floorEntry(months).getValue();
            }
        } catch (DateTimeParseException ex) {
            log.debug("Failed to parse date for MaturityDate field '{}'", maturityDateValue);
        }

        return "";
    }
}
