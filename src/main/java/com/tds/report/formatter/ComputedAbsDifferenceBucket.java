package com.tds.report.formatter;

import java.util.Map;

public class ComputedAbsDifferenceBucket implements ValueExtractor {
    @Override
    public String getValue(Map<String, String> record, String key, String ... fields) {
        try {
            Double val =  Math.abs(Double.parseDouble(record.get(fields[0])) -
                    Double.parseDouble(record.get(fields[1])));

            int exp = val < 1d ? 0 : (int) Math.floor(Math.log10(val));

            switch (exp) {
                case 0:
                case 1: return "0-99";
                case 2: return "100-999";
                case 3: return "1000-9999";
                case 4: return "10000-99999";
                default: return "100000+";
            }
        } catch (NumberFormatException | NullPointerException e) {
            return "";
        }
    }
}
