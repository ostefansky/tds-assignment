package com.tds.report.formatter;

import java.util.Map;

public class DefaultValueExtractor implements ValueExtractor {
    @Override
    public String getValue(Map<String, String> record, String key, String ... fields) {
        return record.getOrDefault(key, "");
    }
}
