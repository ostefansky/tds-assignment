package com.tds.report.formatter;

import java.math.BigDecimal;
import java.util.Map;

public class ComputedFieldsDifference implements ValueExtractor {
    @Override
    public String getValue(Map<String, String> record, String key, String ... fields) {
        try {
            return new BigDecimal(record.get(fields[0]))
                    .subtract(new BigDecimal(record.get(fields[1]))).toString();
        } catch (NumberFormatException | NullPointerException e) {
            return "";
        }
    }
}
