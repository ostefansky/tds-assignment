package com.tds.report.formatter;

import java.util.Map;

public interface ValueExtractor {
    String getValue(Map<String, String> record, String key, String ... customFields);
}
