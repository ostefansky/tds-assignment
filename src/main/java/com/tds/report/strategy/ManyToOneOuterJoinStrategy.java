package com.tds.report.strategy;

import com.tds.report.datasource.DataSource;
import com.tds.report.datasource.InMemoryDataSource;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
public class ManyToOneOuterJoinStrategy implements JoinStrategy {
    @Override
    public DataSource perform(DataSource left, DataSource right, String keyLeft, String keyRight) {
        Optional<Map<String, String>> firstRow = right.getData().stream().findFirst();
        if (firstRow.isPresent()) {
            Set<String> headers = firstRow.get().keySet();
            if (headers.contains(keyRight)) {

                // keyRight Value is expected to be unique within the right datasource
                Map<String, Map<String, String>> rightMap = right.getData().stream().
                        collect(Collectors.toMap(r -> r.get(keyRight), Function.identity()));

                Collection<Map<String, String>> result = new ArrayList<>(left.getData());

                Map<String, String> emptyRight = headers.stream()
                        .filter(h -> !keyRight.equals(h))
                        .collect(Collectors.toMap(s -> s, s -> ""));

                result.forEach(l -> l.putAll(rightMap.getOrDefault(l.get(keyLeft), emptyRight)));

                return new InMemoryDataSource(result);
            } else {
                log.warn("Key column '{}' is missing. Skipping merge.", keyRight);
                return left;
            }
        } else {
            log.warn("Second data source is empty. Skipping merge.");
            return left;
        }
    }

}
