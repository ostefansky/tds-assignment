package com.tds.report.strategy;

import com.tds.report.datasource.DataSource;

public interface JoinStrategy {
    DataSource perform(DataSource left, DataSource right, String keyLeft, String keyRight);
}
