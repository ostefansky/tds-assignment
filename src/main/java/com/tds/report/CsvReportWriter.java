package com.tds.report;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Slf4j
public class CsvReportWriter {
    public void saveToFile(List<List<String>> records, String fileName) {
        try (
                BufferedWriter writer = Files.newBufferedWriter(Paths.get(fileName));
                CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT);
        ) {
            for (List<String> record: records) {
                csvPrinter.printRecord(record);
            }
        } catch (IOException ex) {
            log.error("Error writing report to file '{}': {}", fileName, ex.getMessage());
        }
    }
}
