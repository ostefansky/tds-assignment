package com.tds.report;


import com.tds.report.formatter.ValueExtractor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Column {
    private String name;
    private ValueExtractor formatter;
    private String[] computedFields;
}
