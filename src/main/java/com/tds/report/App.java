package com.tds.report;

import com.tds.report.datasource.CsvDataSource;
import com.tds.report.datasource.DataSource;
import com.tds.report.formatter.DefaultValueExtractor;
import com.tds.report.formatter.MonthsFromTodayBucket;
import com.tds.report.formatter.ComputedAbsDifferenceBucket;
import com.tds.report.formatter.ComputedFieldsDifference;
import com.tds.report.strategy.ManyToOneOuterJoinStrategy;

import java.util.Arrays;
import java.util.stream.Collectors;

public class App {
    public static void main(String[] args) {
        DataSource ds = new ManyToOneOuterJoinStrategy()
                .perform(new CsvDataSource("trade.csv"),
                        new CsvDataSource("refdata.csv"), "Inventory", "Inventory");
        ds = new ManyToOneOuterJoinStrategy()
                .perform(ds,
                        new CsvDataSource("valuation.csv"), "TradeId", "TradeId");

        String[] columns = {"topOfHouse", "segment", "viceChair", "globalBusiness", "Policy", "desk", "portfolio", "BU",
                "CLINE", "Inventory", "Book", "System", "LegalEntity", "TradeId", "Version", "TradeStatus",
                "ProductType", "Resetting Leg", "ProductSubType", "TDSProductType", "SecCodeSubType", "SwapType",
                "Description", "TradeDate", "StartDate", "MaturityDate", "UQL_OC_MMB_MS", "UQL_OC_MMB_MS_PC"
        };
        Report report = Report.builder()
                .dataSource(ds)
                .columns(Arrays.stream(columns).map(c -> Column.builder().name(c).formatter(new DefaultValueExtractor()).build())
                        .collect(Collectors.toList())).build();
        report.getColumns().add(Column.builder().name("MS-PC")
                .formatter(new ComputedFieldsDifference())
                .computedFields(new String[] {"UQL_OC_MMB_MS", "UQL_OC_MMB_MS_PC"}).build());
        report.getColumns().add(Column.builder().name("BreakStatus")
                .formatter(new ComputedAbsDifferenceBucket())
                .computedFields(new String[] {"UQL_OC_MMB_MS", "UQL_OC_MMB_MS_PC"}).build());
        report.getColumns().add(Column.builder().name("Term").
                formatter(new MonthsFromTodayBucket())
                .computedFields(new String[] {"MaturityDate"}).build());

        new CsvReportWriter().saveToFile(report.render(true), args.length > 0 ? args[0] : "out.csv");
    }
}
