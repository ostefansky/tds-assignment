package com.tds.report.datasource;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
public class CsvDataSource implements DataSource {

    private Collection<Map<String, String>> data;

    public CsvDataSource(String fileLocation) {
        try (Reader in = new InputStreamReader(getClass().getClassLoader().getResourceAsStream(fileLocation))) {
            CSVParser parser = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(in);
            data = parser.getRecords().stream()
                    .map(record -> record.toMap())
                    .collect(Collectors.toList());
        } catch (IOException | NullPointerException e) {
            log.error("Failed to parse CSV file from {}", fileLocation, e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public Collection<Map<String, String>> getData() {
        return data;
    }
}
