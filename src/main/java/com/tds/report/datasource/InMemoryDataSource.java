package com.tds.report.datasource;

import java.util.Collection;
import java.util.Map;

public class InMemoryDataSource implements DataSource {
    private Collection<Map<String, String>> data;

    public InMemoryDataSource(Collection<Map<String, String>> data) {
        this.data = data;
    }

    @Override
    public Collection<Map<String, String>> getData() {
        return data;
    }
}
