package com.tds.report.datasource;

import java.util.Collection;
import java.util.Map;

public interface DataSource {
    Collection<Map<String, String>> getData();
}
