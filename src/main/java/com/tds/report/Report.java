package com.tds.report;

import com.tds.report.datasource.DataSource;
import lombok.Builder;
import lombok.Data;

import java.util.*;
import java.util.stream.Collectors;

@Data
@Builder
public class Report {
    private DataSource dataSource;
    private List<Column> columns;

    public List<List<String>> render(boolean withHeader) {
        List<List<String>> result = new ArrayList<>();
        if (withHeader) {
            result.add(columns.stream().map(c -> c.getName()).collect(Collectors.toList()));
        }
        for (Map<String, String> row: dataSource.getData()) {
            List<String> out = new ArrayList<>();
            for (Column col: columns) {
                out.add(col.getFormatter().getValue(row, col.getName(), col.getComputedFields()));
            }
            result.add(out);
        }

        return result;
    }
}
