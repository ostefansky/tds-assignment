package com.tds.report.formatter;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class ValueExtractorTest {
    @Test
    public void testDefaultValueExtractor() {
        Map<String, String> map = new HashMap<>();
        map.put("field1", "value1");
        map.put("field2", "value2");

        String val1 = new DefaultValueExtractor().getValue(map, "field1");
        String val2 = new DefaultValueExtractor().getValue(map, "field2");
        String val3 = new DefaultValueExtractor().getValue(map, "field3");

        Assert.assertEquals("value1", val1);
        Assert.assertEquals("value2", val2);
        Assert.assertEquals("", val3);
    }

    @Test
    public void testCustomDiffExtractor() {
        Map<String, String> map = new HashMap<>();
        map.put("UQL_OC_MMB_MS", "123");
        map.put("UQL_OC_MMB_MS_PC", "value2");

        String val1 = new ComputedFieldsDifference().getValue(map, "field1", "UQL_OC_MMB_MS", "UQL_OC_MMB_MS_PC");
        Assert.assertEquals("", val1);

        map = new HashMap<>();
        map.put("UQL_OC_MMB_MS", "123.45");
        map.put("UQL_OC_MMB_MS_PC", "12.34");

        String val2 = new ComputedFieldsDifference().getValue(map, "field1", "UQL_OC_MMB_MS", "UQL_OC_MMB_MS_PC");
        Assert.assertEquals("111.11", val2);

        map = new HashMap<>();
        map.put("UQL_OC_MMB_MS", "12.34");
        map.put("UQL_OC_MMB_MS_PC", "123.45");

        String val3 = new ComputedFieldsDifference().getValue(map, "field1", "UQL_OC_MMB_MS", "UQL_OC_MMB_MS_PC");
        Assert.assertEquals("-111.11", val3);
    }

    @Test
    public void testComputedAbsDifferenceBucketExtractor() {
        Map<String, String> map = new HashMap<>();
        map.put("UQL_OC_MMB_MS", "0");
        map.put("UQL_OC_MMB_MS_PC", "value2");

        String val1 = new ComputedAbsDifferenceBucket()
                .getValue(map, "field1", "UQL_OC_MMB_MS", "UQL_OC_MMB_MS_PC");
        Assert.assertEquals("", val1);

        map = new HashMap<>();
        map.put("UQL_OC_MMB_MS", "0");
        map.put("UQL_OC_MMB_MS_PC", "0");

        String val0 = new ComputedAbsDifferenceBucket()
                .getValue(map, "field1", "UQL_OC_MMB_MS", "UQL_OC_MMB_MS_PC");
        Assert.assertEquals("0-99", val0);

        map = new HashMap<>();
        map.put("UQL_OC_MMB_MS", "123.45");
        map.put("UQL_OC_MMB_MS_PC", "12.34");

        String val2 = new ComputedAbsDifferenceBucket()
                .getValue(map, "field1", "UQL_OC_MMB_MS", "UQL_OC_MMB_MS_PC");
        Assert.assertEquals("100-999", val2);

        map = new HashMap<>();
        map.put("UQL_OC_MMB_MS", "0");
        map.put("UQL_OC_MMB_MS_PC", "1000");

        String val3 = new ComputedAbsDifferenceBucket()
                .getValue(map, "field1", "UQL_OC_MMB_MS", "UQL_OC_MMB_MS_PC");
        Assert.assertEquals("1000-9999", val3);


        map = new HashMap<>();
        map.put("UQL_OC_MMB_MS", "0");
        map.put("UQL_OC_MMB_MS_PC", "99999");

        val3 = new ComputedAbsDifferenceBucket()
                .getValue(map, "field1", "UQL_OC_MMB_MS", "UQL_OC_MMB_MS_PC");
        Assert.assertEquals("10000-99999", val3);

        map = new HashMap<>();
        map.put("UQL_OC_MMB_MS", "0");
        map.put("UQL_OC_MMB_MS_PC", "1234567");

        val3 = new ComputedAbsDifferenceBucket()
                .getValue(map, "field1", "UQL_OC_MMB_MS", "UQL_OC_MMB_MS_PC");
        Assert.assertEquals("100000+", val3);
    }

    @Test
    public void testMonthsFromTodayBucketExtractor() {
        // missing data
        Map<String, String> map = new HashMap<>();
        map.put("UQL_OC_MMB_MS", "0");
        map.put("UQL_OC_MMB_MS_PC", "value2");

        String val1 = new MonthsFromTodayBucket()
                .getValue(map, "field1", "UQL_OC_MMB_MS");
        Assert.assertEquals("", val1);

        // date in the past
        map = new HashMap<>();
        map.put("MaturityDate", getMonthsFromToday(-1));

        String val = new MonthsFromTodayBucket()
                .getValue(map, "field1", "MaturityDate");
        Assert.assertEquals("", val);

        // date is today
        map = new HashMap<>();
        map.put("MaturityDate", getMonthsFromToday(0));

        val = new MonthsFromTodayBucket()
                .getValue(map, "field1", "MaturityDate");
        Assert.assertEquals("", val);

        // date is 1 mon from now
        map = new HashMap<>();
        map.put("MaturityDate", getMonthsFromToday(1));

        val = new MonthsFromTodayBucket()
                .getValue(map, "field1", "MaturityDate");
        Assert.assertEquals("1m-6m", val);

        // date is 6 months from now
        map = new HashMap<>();
        map.put("MaturityDate", getMonthsFromToday(6));

        val = new MonthsFromTodayBucket()
                .getValue(map, "field1", "MaturityDate");
        Assert.assertEquals("6m-1yr", val);

        // date is 12 months from now
        map = new HashMap<>();
        map.put("MaturityDate", getMonthsFromToday(12));

        val = new MonthsFromTodayBucket()
                .getValue(map, "field1", "MaturityDate");
        Assert.assertEquals("1yr-10yr", val);

        // date is 50 years from now
        map = new HashMap<>();
        map.put("MaturityDate", getMonthsFromToday(602));

        val = new MonthsFromTodayBucket()
                .getValue(map, "field1", "MaturityDate");
        Assert.assertEquals("50yr+", val);
    }

    private String getMonthsFromToday(int months) {
        final DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyyMMdd");
        return LocalDate.now().plusMonths(months).format(format);
    }
}
