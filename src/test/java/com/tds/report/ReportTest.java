package com.tds.report;

import com.tds.report.datasource.DataSource;
import com.tds.report.datasource.InMemoryDataSource;
import com.tds.report.formatter.DefaultValueExtractor;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReportTest {
    @Test
    public void testSimpleReport() {
        List<Column> columns = new ArrayList<>();
        columns.add(Column.builder().name("data1").formatter(new DefaultValueExtractor()).build());
        columns.add(Column.builder().name("key").formatter(new DefaultValueExtractor()).build());

        Report rpt = Report.builder().dataSource(getMainDs()).columns(columns).build();
        List<List<String>> records = rpt.render(true);

        // three records with header
        Assert.assertEquals(3, records.size());
        // two columns
        Assert.assertEquals(2, records.get(0).size());
        // data
        Assert.assertEquals("data1", records.get(0).get(0));
        Assert.assertEquals("key", records.get(0).get(1));
        Assert.assertEquals("data", records.get(1).get(0));
        Assert.assertEquals("123", records.get(1).get(1));
        Assert.assertEquals("data2", records.get(2).get(0));
        Assert.assertEquals("123", records.get(2).get(1));
    }

    private DataSource getMainDs() {
        List<Map<String, String>> list = new ArrayList<>();
        Map<String, String> map = new HashMap<>();
        map.put("key", "123");
        map.put("data1", "data");
        list.add(map);

        map = new HashMap<>();
        map.put("key", "123");
        map.put("data1", "data2");
        list.add(map);

        return new InMemoryDataSource(list);
    }

}
