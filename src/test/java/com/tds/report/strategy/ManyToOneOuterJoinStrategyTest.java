package com.tds.report.strategy;

import static org.junit.Assert.*;

import com.tds.report.datasource.DataSource;
import com.tds.report.datasource.InMemoryDataSource;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ManyToOneOuterJoinStrategyTest {
    @Test
    public void testManyToOne(){
        DataSource ds = new ManyToOneOuterJoinStrategy().perform(getMainDs(), getDetailDsMatching(), "key", "key");

        assertNotNull(ds.getData());
        assertEquals(2, ds.getData().size());
        for (Map<String, String> map: ds.getData()) {
            assertEquals("detail", map.get("data2"));
        }
    }

    @Test
    public void testManyToOneOuter(){
        DataSource ds = new ManyToOneOuterJoinStrategy().perform(getMainDs(), getDetailDsNotMatching(), "key", "key");

        assertNotNull(ds.getData());
        assertEquals(2, ds.getData().size());
        for (Map<String, String> map: ds.getData()) {
            assertEquals("", map.getOrDefault("data2", ""));
        }
    }

    @Test(expected = IllegalStateException.class)
    public void testDetailDuplicates(){
        DataSource ds = new ManyToOneOuterJoinStrategy().perform(getMainDs(), getDetailDsWithDuplicates(), "key", "key");
    }


    private DataSource getMainDs() {
        List<Map<String, String>> list = new ArrayList<>();
        Map<String, String> map = new HashMap<>();
        map.put("key", "123");
        map.put("data1", "data");
        list.add(map);

        map = new HashMap<>();
        map.put("key", "123");
        map.put("data1", "data2");
        list.add(map);

        return new InMemoryDataSource(list);
    }

    private DataSource getDetailDsMatching() {
        List<Map<String, String>> list = new ArrayList<>();
        Map<String, String> map = new HashMap<>();
        map.put("key", "123");
        map.put("data2", "detail");
        list.add(map);

        return new InMemoryDataSource(list);
    }

    private DataSource getDetailDsNotMatching() {
        List<Map<String, String>> list = new ArrayList<>();
        Map<String, String> map = new HashMap<>();
        map.put("key", "234");
        map.put("data2", "detail");
        list.add(map);

        return new InMemoryDataSource(list);
    }

    private DataSource getDetailDsWithDuplicates() {
        List<Map<String, String>> list = new ArrayList<>();
        Map<String, String> map = new HashMap<>();
        map.put("key", "123");
        map.put("data2", "detail1");
        list.add(map);

        map = new HashMap<>();
        map.put("key", "123");
        map.put("data2", "detail2");
        list.add(map);

        return new InMemoryDataSource(list);
    }
}
