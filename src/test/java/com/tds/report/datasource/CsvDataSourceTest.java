package com.tds.report.datasource;

import static org.junit.Assert.*;
import org.junit.Test;

import java.util.Map;

public class CsvDataSourceTest {
    @Test
    public void testCsvReader() {
        DataSource ds = new CsvDataSource("test.csv");
        assertNotNull(ds.getData());
        assertEquals(1, ds.getData().size());
        for (Map<String, String> map: ds.getData()) {
            assertEquals("data", map.get("col1"));
            assertEquals("coma escaping, test", map.get("col2"));
        }
    }

    @Test(expected = RuntimeException.class)
    public void testMissingFile() {
        DataSource ds = new CsvDataSource("noexistentds");
        assertNotNull(ds.getData());
        assertEquals(1, ds.getData().size());
        for (Map<String, String> map: ds.getData()) {
            assertEquals("data", map.get("col1"));
            assertEquals("coma escaping, test", map.get("col2"));
        }
    }
}
